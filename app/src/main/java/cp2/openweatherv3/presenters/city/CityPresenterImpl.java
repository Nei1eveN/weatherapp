package cp2.openweatherv3.presenters.city;

import android.content.Context;
import android.util.Log;

import cp2.openweatherv3.models.city.CityWeatherData;
import io.realm.RealmResults;

public class CityPresenterImpl implements CityPresenter, CityInteractor.OnSyncDataListener {
    CityPresenter.View view;
    CityInteractorImpl interactor;

    public CityPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new CityInteractorImpl(context, this);
    }

    @Override
    public void requestNetworkCondition() {
        Log.d("presenter", "get condition from interactor");
        interactor.getNetworkCondition();
    }

    @Override
    public void requestLocation(String lat, String lon) {
        interactor.getCurrentLocation(lat, lon);
    }

    @Override
    public void setDataNotFound() {
        Log.d("onSyncDataNotFound", "no data found, go back to view");
        view.noDataFound();
    }

    @Override
    public void setDataFound(RealmResults<CityWeatherData> weatherData) {
        Log.d("onSyncDataFound", "there is something from interactor, send this weatherData to view");
        view.setDataToRecycler(weatherData);
    }
}
