package cp2.openweatherv3.presenters.city;


import cp2.openweatherv3.models.city.CityWeatherData;
import io.realm.RealmResults;

public interface CityInteractor {
    void getNetworkCondition();
    void getOfflineData();
    void getCurrentWeather();
    void getCurrentLocation(String lat, String lon);
    interface OnSyncDataListener{
        void setDataNotFound();
        void setDataFound(RealmResults<CityWeatherData> weatherData);
    }
}
