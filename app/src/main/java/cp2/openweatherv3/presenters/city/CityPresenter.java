package cp2.openweatherv3.presenters.city;

import cp2.openweatherv3.models.city.CityWeatherData;
import io.realm.RealmResults;

public interface CityPresenter {
    interface View{
        void showProgress();
        void hideProgress();
        void setDataToRecycler(final RealmResults<CityWeatherData> weatherData);
        void noDataFound();
    }
    void requestNetworkCondition();
    void requestLocation(String lat, String lon);
}
