package cp2.openweatherv3.presenters.city;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import cp2.openweatherv3.models.city.CityWeatherData;
import cp2.openweatherv3.models.location.LocationWeatherData;
import cp2.openweatherv3.services.APIService;
import cp2.openweatherv3.utils.APIClient;
import cp2.openweatherv3.utils.Constants;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class CityInteractorImpl implements CityInteractor {
    Context context;
    Realm realm = Realm.getDefaultInstance();
    RealmResults<CityWeatherData> weatherData;
    OnSyncDataListener listener;

    public CityInteractorImpl(Context context) {
        this.context = context;
    }

    public CityInteractorImpl(Context context, OnSyncDataListener listener) { //CityPresenter presenter
        this.context = context;
        this.listener = listener;
    }

    @Override
    public void getNetworkCondition() {
        if (Constants.isNetworkAvailable(context)){
            Log.d("interactor", "network is available, getCurrentWeather");
            getCurrentWeather();
        } else {
            Log.d("interactor", "network is not available, getOfflineData");
            getOfflineData();
        }
    }

    @Override
    public void getOfflineData() {
        weatherData = realm.where(CityWeatherData.class).findAll();
        if (weatherData.isEmpty()){
            Log.d("interactor", "weatherData is empty, go back to presenter");
//            presenter.setNoDataToView();
            listener.setDataNotFound();
        }
        else {
            Log.d("interactor", "weatherData has something, go back to presenter");
//            presenter.setRecentDataToView(weatherData);
            listener.setDataFound(weatherData);
        }
    }

    @Override
    public void getCurrentWeather() {
        Log.d("interactor", "getCurrentWeather something");
        weatherData = realm.where(CityWeatherData.class).findAll();
        APIService apiService = APIClient.getClient(context).create(APIService.class);
        RealmList<String> stringRealmList = new RealmList<>();
        for (int i = 0; i < weatherData.size(); i++){
            stringRealmList.add(weatherData.get(i).getName());
        }
        for (String cities : stringRealmList){
            Call<CityWeatherData> weatherDataCall = apiService.getWeatherData(cities, Constants.API_KEY);
            weatherDataCall.enqueue(new Callback<CityWeatherData>() {
                @Override
                public void onResponse(Call<CityWeatherData> call, Response<CityWeatherData> response) {
                    CityWeatherData weatherData = response.body();
                    Log.e("interactor -- id/s", weatherData.getId().toString());
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(weatherData);
                    realm.commitTransaction();
                }

                @Override
                public void onFailure(Call<CityWeatherData> call, Throwable t) {
                    Log.wtf("throw", t.getLocalizedMessage());
                }
            });
        }
        getOfflineData();
    }

    @Override
    public void getCurrentLocation(String lat, String lon) {
        if (Constants.isNetworkAvailable(context)){
            APIService apiService = APIClient.getClient(context).create(APIService.class);
            Call<LocationWeatherData> locationWeatherDataCall = apiService.getLocationData(lat, lon, Constants.API_KEY);
            locationWeatherDataCall.enqueue(new Callback<LocationWeatherData>() {
                @Override
                public void onResponse(Call<LocationWeatherData> call, Response<LocationWeatherData> response) {
                    LocationWeatherData locationWeatherData = response.body();
                    Log.d("currentCity", locationWeatherData.getName());
                }

                @Override
                public void onFailure(Call<LocationWeatherData> call, Throwable t) {

                }
            });
        }
        else {
            Toast.makeText(context, "Connection not available", Toast.LENGTH_SHORT).show();
        }
    }
}