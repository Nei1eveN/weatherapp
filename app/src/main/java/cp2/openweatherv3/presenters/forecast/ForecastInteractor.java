package cp2.openweatherv3.presenters.forecast;

public interface ForecastInteractor {
    void getNetworkCondition(Integer cityId);
    interface CurrentData{
        void getCurrentData(Integer cityId);
        void getForecastData(Integer cityId);
    }
    interface OfflineData{
        void getOfflineCurrentData(Integer cityId);
        void getOfflineForecastData(Integer cityId);
    }
}
