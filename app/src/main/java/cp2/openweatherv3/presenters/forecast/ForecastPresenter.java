package cp2.openweatherv3.presenters.forecast;

import cp2.openweatherv3.adapters.ForecastSectionRecyclerViewAdapter;
import cp2.openweatherv3.models.city.CityWeatherData;
import cp2.openweatherv3.models.forecast.ForecastData;
import io.realm.RealmResults;

public interface ForecastPresenter {
    interface View{
        void setCurrentWeatherData(RealmResults<CityWeatherData> cityWeatherData);
        void setForecastData(RealmResults<ForecastData> forecastData);
        void showProgress(String caption);
        void hideProgress();
        ForecastSectionRecyclerViewAdapter getSectionAdapter(int id);
    }
    void requestCurrentData(Integer cityId);
    void setCurrentDataToView(RealmResults<CityWeatherData> cityWeatherDataRealmResults);
    void setForecastDataToView(RealmResults<ForecastData> forecastData);
}
