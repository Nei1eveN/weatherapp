package cp2.openweatherv3.presenters.forecast;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import cp2.openweatherv3.models.city.CityWeatherData;
import cp2.openweatherv3.models.forecast.ForecastData;
import cp2.openweatherv3.services.APIService;
import cp2.openweatherv3.utils.APIClient;
import cp2.openweatherv3.utils.Constants;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cp2.openweatherv3.utils.Constants.API_KEY;

class ForecastInteractorImpl implements ForecastInteractor, ForecastInteractor.CurrentData, ForecastInteractor.OfflineData {
    Context context;
    ForecastPresenterImpl forecastPresenter;
    Realm realm = Realm.getDefaultInstance();

    ForecastInteractorImpl(Context context, ForecastPresenterImpl forecastPresenter) {
        this.context = context;
        this.forecastPresenter = forecastPresenter;
    }

    @Override
    public void getCurrentData(final Integer cityId) {
        APIService weatherService = APIClient.getClient(context).create(APIService.class);
        Call<CityWeatherData> weatherDataCall = weatherService.getWeatherDataById(cityId, API_KEY);
        weatherDataCall.enqueue(new Callback<CityWeatherData>() {
            @Override
            public void onResponse(@NonNull Call<CityWeatherData> call, @NonNull Response<CityWeatherData> response) {
                CityWeatherData weatherData = response.body();
                Log.i("interactor-weatherName", weatherData.getName());
                Log.w("interactor-realmUpdate", "saving/updating data to realm: " + weatherData.getName());
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(weatherData);
                realm.commitTransaction();
                getOfflineCurrentData(cityId);
            }

            @Override
            public void onFailure(@NonNull Call<CityWeatherData> call, @NonNull Throwable t) {
                Log.d("forecastInteractorError", "Current Data error: " + t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void getOfflineCurrentData(Integer cityId) {
        RealmResults<CityWeatherData> weatherDataRealmResults = realm.where(CityWeatherData.class).equalTo("id", cityId).findAll();
        forecastPresenter.setCurrentDataToView(weatherDataRealmResults);
    }

    @Override
    public void getForecastData(final Integer cityId) {
        APIService forecastService = APIClient.getClient(context).create(APIService.class);
        Call<ForecastData> forecastDataCall = forecastService.getForecastDataById(cityId, API_KEY);
        forecastDataCall.enqueue(new Callback<ForecastData>() {
            @Override
            public void onResponse(@NonNull Call<ForecastData> call, @NonNull Response<ForecastData> response) {
                ForecastData forecastData = response.body();
                forecastData.setForecastId(forecastData.getForecastCity().getId());
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(forecastData);
                realm.commitTransaction();
                getOfflineForecastData(cityId);
            }

            @Override
            public void onFailure(@NonNull Call<ForecastData> call, @NonNull Throwable t) {
                Log.d("forecastDataOnFailure", "failed: " + t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void getOfflineForecastData(Integer cityId) {
        /**
         * Find the forecastId to RealmResults, finding all data that contains that ID to be sent back to our presenter
         * **/
        RealmResults<ForecastData> forecastData = realm.where(ForecastData.class).equalTo("forecastId", cityId).findAll();
        Log.d("interactor-offlForecast", "passing " + cityId + " to presenter");
        forecastPresenter.setForecastDataToView(forecastData);
    }

    @Override
    public void getNetworkCondition(Integer cityId) {
        if (Constants.isNetworkAvailable(context)) {
            getCurrentData(cityId);
            getForecastData(cityId);
        } else {
            Log.d("networkCond", "this offline data");
            getOfflineCurrentData(cityId);
            getOfflineForecastData(cityId);
        }
    }
}