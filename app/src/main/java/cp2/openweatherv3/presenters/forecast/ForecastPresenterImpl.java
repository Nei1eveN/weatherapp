package cp2.openweatherv3.presenters.forecast;

import android.content.Context;
import android.util.Log;

import cp2.openweatherv3.models.city.CityWeatherData;
import cp2.openweatherv3.models.forecast.ForecastData;
import io.realm.RealmResults;

public class ForecastPresenterImpl implements ForecastPresenter {
    ForecastPresenter.View view;
    ForecastInteractorImpl forecastInteractor;

    public ForecastPresenterImpl(View view, Context context) {
        this.view = view;
        this.forecastInteractor = new ForecastInteractorImpl(context, this);
    }

    @Override
    public void requestCurrentData(Integer cityId) {
        view.showProgress("Please wait...");
        Log.d("forecastPresenter", "request data condition from Interactor");
        forecastInteractor.getNetworkCondition(cityId);
    }

    @Override
    public void setCurrentDataToView(RealmResults<CityWeatherData> cityWeatherDataRealmResults) {
        Log.d("forecastPresenter", "sending Current Data to view");
        view.setCurrentWeatherData(cityWeatherDataRealmResults);
        view.hideProgress();
    }

    @Override
    public void setForecastDataToView(RealmResults<ForecastData> forecastToday) {
        Log.d("forecastPresenter", "sending Forecast Data to view");
        view.setForecastData(forecastToday);
        view.hideProgress();
    }
}
