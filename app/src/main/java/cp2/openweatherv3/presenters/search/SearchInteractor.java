package cp2.openweatherv3.presenters.search;

import cp2.openweatherv3.models.search.SearchListData;

public interface SearchInteractor {
    void getData(String city, OnSyncListener listener);
    interface OnSyncListener{
        void onSuccess(SearchListData searchListData);
        void onFailure(Throwable t);
    }
}
