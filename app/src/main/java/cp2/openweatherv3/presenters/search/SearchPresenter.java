package cp2.openweatherv3.presenters.search;

import java.util.List;

import cp2.openweatherv3.models.search.SearchList;

public interface SearchPresenter {
    interface View{
        void generateList(List<SearchList> list);
    }
    void requestData(String city);
}
