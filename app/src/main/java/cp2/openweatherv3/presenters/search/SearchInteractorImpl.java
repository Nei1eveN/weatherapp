package cp2.openweatherv3.presenters.search;

import android.content.Context;
import android.util.Log;

import cp2.openweatherv3.models.search.SearchListData;
import cp2.openweatherv3.services.APIService;
import cp2.openweatherv3.utils.APIClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cp2.openweatherv3.utils.Constants.API_KEY;

public class SearchInteractorImpl implements SearchInteractor {
    Context context;

    public SearchInteractorImpl(Context context) {
        //SearchPresenter searchPresenter can be added here as part of constructor whenever you are confused
        this.context = context;
    }

    @Override
    public void getData(String city, OnSyncListener listener) {
            APIService listService = APIClient.getClient(context).create(APIService.class);
            Call<SearchListData> resultsCall = listService.getListData(city,API_KEY);
            resultsCall.enqueue(new Callback<SearchListData>() {
                @Override
                public void onResponse(Call<SearchListData> call, Response<SearchListData> response) {
                    SearchListData searchListData = response.body();

                    listener.onSuccess(searchListData);
                }

                @Override
                public void onFailure(Call<SearchListData> call, Throwable t) {
                    Log.d("presInteractorFailed", t.getMessage());
                    listener.onFailure(t);
                }
            });

    }
}