package cp2.openweatherv3.presenters.search;

import android.content.Context;
import android.util.Log;

import cp2.openweatherv3.models.search.SearchListData;

public class SearchPresenterImpl implements SearchPresenter, SearchInteractor.OnSyncListener {
    SearchPresenter.View view;
    SearchInteractor searchInteractor;

    public SearchPresenterImpl(View view, Context context) {
        this.view = view;
        this.searchInteractor = new SearchInteractorImpl(context); //, this
    }

    @Override
    public void requestData(String city) {
        if (city.length() >= 3) {
            searchInteractor.getData(city, this);
        }
        else {
            Log.d("presenterReqData", "do nothing");
        }
    }

    @Override
    public void onSuccess(SearchListData searchListData) {
        Log.d("onSyncSuccess", searchListData.getMessage());
        view.generateList(searchListData.getSearchList());
    }

    @Override
    public void onFailure(Throwable t) {
        Log.d("onSyncFailure", t.getMessage());
    }
}
