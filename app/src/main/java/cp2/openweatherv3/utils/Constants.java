package cp2.openweatherv3.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import cp2.openweatherv3.R;

public class Constants {
    public static String url = "http://api.openweathermap.org/";
    public static String API_KEY = "3e86823b40460a5fd131d06ec418df03";
    public static int REQUEST_CODE = 0;
    public static int cacheSize = 10 * 1024 * 1024;
    public static long UPDATE_INTERVAL = 15000;  /* 15 secs */
    public static long FASTEST_INTERVAL = 5000; /* 5 secs */
    public static int ALL_PERMISSIONS_RESULT = 101;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;

    public static final int REQUEST_PERMISSIONS_LOCATION_SETTINGS_REQUEST_CODE = 33;
    public static final int REQUEST_PERMISSIONS_LAST_LOCATION_REQUEST_CODE = 34;
    public static final int REQUEST_PERMISSIONS_CURRENT_LOCATION_REQUEST_CODE = 35;
    public static long MIN_UPDATE_INTERVAL = 30 * 1000; //1 minute is minimum Android recommends, but we use 30 seconds


    public static int getWeatherIcon(String weatherIconCode) {
        switch (weatherIconCode) {
            case "01d":
                return R.drawable.clear_day;
            case "01n":
                return R.drawable.clear_night;
            case "02d":
                return R.drawable.partly_cloudy;
            case "02n":
                return R.drawable.partly_cloudy_night;
            case "03d":
                return R.drawable.cloudy_weather;
            case "03n":
                return R.drawable.cloudy_weather;
            case "04d":
                return R.drawable.cloudy_weather;
            case "04n":
                return R.drawable.cloudy_weather;
            case "09d":
                return R.drawable.rainy_day;
            case "09n":
                return R.drawable.rainy_night;
            case "10d":
                return R.drawable.rainy_day;
            case "10n":
                return R.drawable.rainy_night;
            case "11d":
                return R.drawable.thunder_day;
            case "11n":
                return R.drawable.thunder_night;
            case "13d":
                return R.drawable.snow_day;
            case "13n":
                return R.drawable.snow_night;
            case "50d":
                return R.drawable.haze_day;
            case "50n":
                return R.drawable.haze_night;
            default:
                return R.drawable.clear_day;
        }
    }
    public static String convertDegreeToCardinalDirection(long directionInDegrees){
        String cardinalDirection;
        if( (directionInDegrees >= 348.75) && (directionInDegrees <= 360) ||
                (directionInDegrees >= 0) && (directionInDegrees <= 11.25)    ){
            cardinalDirection = "N";
        } else if( (directionInDegrees >= 11.25 ) && (directionInDegrees <= 33.75)){
            cardinalDirection = "NNE";
        } else if( (directionInDegrees >= 33.75 ) &&(directionInDegrees <= 56.25)){
            cardinalDirection = "NE";
        } else if( (directionInDegrees >= 56.25 ) && (directionInDegrees <= 78.75)){
            cardinalDirection = "ENE";
        } else if( (directionInDegrees >= 78.75 ) && (directionInDegrees <= 101.25) ){
            cardinalDirection = "E";
        } else if( (directionInDegrees >= 101.25) && (directionInDegrees <= 123.75) ){
            cardinalDirection = "ESE";
        } else if( (directionInDegrees >= 123.75) && (directionInDegrees <= 146.25) ){
            cardinalDirection = "SE";
        } else if( (directionInDegrees >= 146.25) && (directionInDegrees <= 168.75) ){
            cardinalDirection = "SSE";
        } else if( (directionInDegrees >= 168.75) && (directionInDegrees <= 191.25) ){
            cardinalDirection = "S";
        } else if( (directionInDegrees >= 191.25) && (directionInDegrees <= 213.75) ){
            cardinalDirection = "SSW";
        } else if( (directionInDegrees >= 213.75) && (directionInDegrees <= 236.25) ){
            cardinalDirection = "SW";
        } else if( (directionInDegrees >= 236.25) && (directionInDegrees <= 258.75) ){
            cardinalDirection = "WSW";
        } else if( (directionInDegrees >= 258.75) && (directionInDegrees <= 281.25) ){
            cardinalDirection = "W";
        } else if( (directionInDegrees >= 281.25) && (directionInDegrees <= 303.75) ){
            cardinalDirection = "WNW";
        } else if( (directionInDegrees >= 303.75) && (directionInDegrees <= 326.25) ){
            cardinalDirection = "NW";
        } else if( (directionInDegrees >= 326.25) && (directionInDegrees <= 348.75) ){
            cardinalDirection = "NNW";
        } else {
            cardinalDirection = "?";
        }

        return cardinalDirection;
    }
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
