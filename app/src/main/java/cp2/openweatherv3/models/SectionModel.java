package cp2.openweatherv3.models;

import cp2.openweatherv3.models.city.CityList;
import cp2.openweatherv3.models.forecast.ForecastList;
import cp2.openweatherv3.models.location.LocationList;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

@RealmClass
public class SectionModel extends RealmObject implements RealmModel{
    String sectionLabel;
    RealmList<ForecastList> forecastListRealmList;
    RealmList<CityList> cityListWeatherData;
    RealmList<LocationList> locationLists;

//    public SectionModel(String sectionLabel, RealmList<ForecastList> forecastListRealmList) {
//        this.sectionLabel = sectionLabel;
//        this.forecastListRealmList = forecastListRealmList;
//    }

    public SectionModel() { }

    public String getSectionLabel() {
        return sectionLabel;
    }

    public void setSectionLabel(String sectionLabel) {
        this.sectionLabel = sectionLabel;
    }

    public RealmList<ForecastList> getForecastListRealmList() {
        return forecastListRealmList;
    }

    public void setForecastListRealmList(RealmList<ForecastList> forecastListRealmList) {
        this.forecastListRealmList = forecastListRealmList;
    }

    public RealmList<CityList> getCityListWeatherData() {
        return cityListWeatherData;
    }

    public void setCityListWeatherData(RealmList<CityList> cityListWeatherData) {
        this.cityListWeatherData = cityListWeatherData;
    }

    public RealmList<LocationList> getLocationLists() {
        return locationLists;
    }

    public void setLocationLists(RealmList<LocationList> locationLists) {
        this.locationLists = locationLists;
    }
}
