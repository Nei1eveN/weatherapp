package cp2.openweatherv3.models.city;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class CityList extends RealmObject {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("dt")
    @Expose
    private Integer dt;
    @SerializedName("main")
    @Expose
    private CityMain main;
    @SerializedName("weather")
    @Expose
    private RealmList<CityWeather> weather = new RealmList<>();
    @SerializedName("clouds")
    @Expose
    private CityClouds clouds;
    @SerializedName("wind")
    @Expose
    private CityWind wind;
    @SerializedName("rain")
    @Expose
    private CityRain rain;
    @SerializedName("sys")
    @Expose
    private CitySys sys;
    @SerializedName("dt_txt")
    @Expose
    private String dtTxt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public CityMain getMain() {
        return main;
    }

    public void setMain(CityMain main) {
        this.main = main;
    }

    public RealmList<CityWeather> getWeather() {
        return weather;
    }

    public void setWeather(RealmList<CityWeather> weather) {
        this.weather = weather;
    }

    public CityClouds getClouds() {
        return clouds;
    }

    public void setClouds(CityClouds clouds) {
        this.clouds = clouds;
    }

    public CityWind getWind() {
        return wind;
    }

    public void setWind(CityWind wind) {
        this.wind = wind;
    }

    public CityRain getRain() {
        return rain;
    }

    public void setRain(CityRain rain) {
        this.rain = rain;
    }

    public CitySys getSys() {
        return sys;
    }

    public void setSys(CitySys sys) {
        this.sys = sys;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }

}
