package cp2.openweatherv3.models.city;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class CityWeatherData extends RealmObject {

    @SerializedName("coord")
    @Expose
    private CityCoord coord;

    @SerializedName("weather")
    @Expose
    private RealmList<CityWeather> weather;

    @SerializedName("base")
    @Expose
    private String base;

    @SerializedName("main")
    @Expose
    private CityMain main;

    @SerializedName("visibility")
    @Expose
    private Integer visibility;

    @SerializedName("wind")
    @Expose
    private CityWind wind;

    @SerializedName("clouds")
    @Expose
    private CityClouds clouds;

    @SerializedName("dt")
    @Expose
    private Integer dt;

    @SerializedName("sys")
    @Expose
    private CitySys sys;

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("cod")
    @Expose
    private Integer cod;

    @SerializedName("count")
    @Expose
    private Integer count;

    @SerializedName("list")
    @Expose
    private RealmList<CityList> list = new RealmList<>();

    public CityCoord getCoord() {
        return coord;
    }

    public void setCoord(CityCoord coord) {
        this.coord = coord;
    }

    public RealmList<CityWeather> getWeather() {
        return weather;
    }

    public void setWeather(RealmList<CityWeather> weather) {
        this.weather = weather;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public CityMain getMain() {
        return main;
    }

    public void setMain(CityMain main) {
        this.main = main;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public CityWind getWind() {
        return wind;
    }

    public void setWind(CityWind wind) {
        this.wind = wind;
    }

    public CityClouds getClouds() {
        return clouds;
    }

    public void setClouds(CityClouds clouds) {
        this.clouds = clouds;
    }

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public CitySys getSys() {
        return sys;
    }

    public void setSys(CitySys sys) {
        this.sys = sys;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCod() {
        return cod;
    }

    public void setCod(Integer cod) {
        this.cod = cod;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public RealmList<CityList> getList() {
        return list;
    }

    public void setList(RealmList<CityList> list) {
        this.list = list;
    }
}