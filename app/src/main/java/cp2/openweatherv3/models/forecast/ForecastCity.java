package cp2.openweatherv3.models.forecast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ForecastCity extends RealmObject {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("coord")
    @Expose
    private ForecastCoord forecastCoord;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("population")
    @Expose
    private Integer population;

    private ForecastData forecastData;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ForecastCoord getForecastCoord() {
        return forecastCoord;
    }

    public void setForecastCoord(ForecastCoord forecastCoord) {
        this.forecastCoord = forecastCoord;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public ForecastData getForecastData() {
        return forecastData;
    }

    public void setForecastData(ForecastData forecastData) {
        this.forecastData = forecastData;
    }
}