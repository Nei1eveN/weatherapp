package cp2.openweatherv3.models.forecast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ForecastData extends RealmObject {

    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("message")
    @Expose
    private Double message;
    @SerializedName("cnt")
    @Expose
    private Integer cnt;
    @SerializedName("list")
    @Expose
    private RealmList<ForecastList> forecastList;
    @SerializedName("city")
    @Expose
    private ForecastCity forecastCity;

    @PrimaryKey
    private Integer forecastId;

    public Integer getForecastId() {
        return forecastId;
    }

    public void setForecastId(Integer forecastId) {
        this.forecastId = forecastId;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public RealmList<ForecastList> getForecastList() {
        return forecastList;
    }

    public void setForecastList(RealmList<ForecastList> forecastList) {
        this.forecastList = forecastList;
    }

    public ForecastCity getForecastCity() {
        return forecastCity;
    }

    public void setForecastCity(ForecastCity forecastCity) {
        this.forecastCity = forecastCity;
    }

}
