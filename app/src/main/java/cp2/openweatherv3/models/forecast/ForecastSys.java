package cp2.openweatherv3.models.forecast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ForecastSys extends RealmObject {
    
        @SerializedName("pod")
        @Expose
        private String pod;
    
        public String getPod() {
            return pod;
        }
    
        public void setPod(String pod) {
            this.pod = pod;
        }
    
    }