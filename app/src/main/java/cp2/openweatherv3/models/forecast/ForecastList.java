package cp2.openweatherv3.models.forecast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ForecastList extends RealmObject {

    @SerializedName("dt")
    @Expose
    private Integer dt;
    @SerializedName("main")
    @Expose
    private ForecastMain forecastMain;
    @SerializedName("weather")
    @Expose
    private RealmList<ForecastWeather> forecastWeather;
    @SerializedName("clouds")
    @Expose
    private ForecastClouds forecastClouds;
    @SerializedName("wind")
    @Expose
    private ForecastWind forecastWind;
    @SerializedName("rain")
    @Expose
    private ForecastRain forecastRain;
    @SerializedName("sys")
    @Expose
    private ForecastSys forecastSys;
    @SerializedName("dt_txt")
    @Expose
    private String dtTxt;

    public Integer cityID;

    public Integer getCityID() {
        return cityID;
    }

    public void setCityID(Integer cityID) {
        this.cityID = cityID;
    }

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public ForecastMain getForecastMain() {
        return forecastMain;
    }

    public void setForecastMain(ForecastMain forecastMain) {
        this.forecastMain = forecastMain;
    }

    public RealmList<ForecastWeather> getForecastWeather() {
        return forecastWeather;
    }

    public void setForecastWeather(RealmList<ForecastWeather> forecastWeather) {
        this.forecastWeather = forecastWeather;
    }

    public ForecastClouds getForecastClouds() {
        return forecastClouds;
    }

    public void setForecastClouds(ForecastClouds forecastClouds) {
        this.forecastClouds = forecastClouds;
    }

    public ForecastWind getForecastWind() {
        return forecastWind;
    }

    public void setForecastWind(ForecastWind forecastWind) {
        this.forecastWind = forecastWind;
    }

    public ForecastRain getForecastRain() {
        return forecastRain;
    }

    public void setForecastRain(ForecastRain forecastRain) {
        this.forecastRain = forecastRain;
    }

    public ForecastSys getForecastSys() {
        return forecastSys;
    }

    public void setForecastSys(ForecastSys forecastSys) {
        this.forecastSys = forecastSys;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }
}