package cp2.openweatherv3.models.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class SearchList extends RealmObject {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("coord")
    @Expose
    private SearchCoord searchCoord;
    @SerializedName("main")
    @Expose
    private SearchMain searchMain;
    @SerializedName("dt")
    @Expose
    private Integer dt;
    @SerializedName("wind")
    @Expose
    private SearchWind searchWind;
    @SerializedName("sys")
    @Expose
    private SearchSys searchSys;
    @SerializedName("rain")
    @Expose
    private SearchRain searchRain;
    //@SerializedName("snow")
//@Expose
//private Snow snow;
    @SerializedName("clouds")
    @Expose
    private SearchClouds searchClouds;
    @SerializedName("weather")
    @Expose
    private RealmList<SearchWeather> searchWeather;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SearchCoord getSearchCoord() {
        return searchCoord;
    }

    public void setSearchCoord(SearchCoord searchCoord) {
        this.searchCoord = searchCoord;
    }

    public SearchMain getSearchMain() {
        return searchMain;
    }

    public void setSearchMain(SearchMain searchMain) {
        this.searchMain = searchMain;
    }

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public SearchWind getSearchWind() {
        return searchWind;
    }

    public void setSearchWind(SearchWind searchWind) {
        this.searchWind = searchWind;
    }

    public SearchSys getSearchSys() {
        return searchSys;
    }

    public void setSearchSys(SearchSys searchSys) {
        this.searchSys = searchSys;
    }

    public SearchRain getSearchRain() {
        return searchRain;
    }

    public void setSearchRain(SearchRain searchRain) {
        this.searchRain = searchRain;
    }

//public Object getSnow() {
//return snow;
//}
//
//public void setSnow(Object snow) {
//this.snow = snow;
//}

    public SearchClouds getSearchClouds() {
        return searchClouds;
    }

    public void setSearchClouds(SearchClouds searchClouds) {
        this.searchClouds = searchClouds;
    }

    public RealmList<SearchWeather> getSearchWeather() {
        return searchWeather;
    }

    public void setSearchWeather(RealmList<SearchWeather> searchWeather) {
        this.searchWeather = searchWeather;
    }
}
