package cp2.openweatherv3.models.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class SearchClouds extends RealmObject{

@SerializedName("all")
@Expose
private Integer all;

public Integer getAll() {
return all;
}

public void setAll(Integer all) {
this.all = all;
}

}