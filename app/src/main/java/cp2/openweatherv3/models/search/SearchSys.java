package cp2.openweatherv3.models.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class SearchSys extends RealmObject{

@SerializedName("country")
@Expose
private String country;

public String getCountry() {
return country;
}

public void setCountry(String country) {
this.country = country;
}

}
