package cp2.openweatherv3.models.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class SearchRain extends RealmObject{

@SerializedName("3h")
@Expose
private Double _3h;

public Double get3h() {
return _3h;
}

public void set3h(Double _3h) {
this._3h = _3h;
}

}