package cp2.openweatherv3.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class LocationClouds extends RealmObject{

@SerializedName("all")
@Expose
private Integer all;

public Integer getAll() {
return all;
}

public void setAll(Integer all) {
this.all = all;
}

}
