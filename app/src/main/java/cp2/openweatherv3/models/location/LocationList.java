package cp2.openweatherv3.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class LocationList extends RealmObject {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("dt")
    @Expose
    private Integer dt;
    @SerializedName("main")
    @Expose
    private LocationMain main;
    @SerializedName("weather")
    @Expose
    private RealmList<LocationWeather> weather = new RealmList<>();
    @SerializedName("clouds")
    @Expose
    private LocationClouds clouds;
    @SerializedName("wind")
    @Expose
    private LocationWind wind;
    @SerializedName("rain")
    @Expose
    private LocationRain rain;
    @SerializedName("sys")
    @Expose
    private LocationSys sys;
    @SerializedName("dt_txt")
    @Expose
    private String dtTxt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public LocationMain getMain() {
        return main;
    }

    public void setMain(LocationMain main) {
        this.main = main;
    }

    public RealmList<LocationWeather> getWeather() {
        return weather;
    }

    public void setWeather(RealmList<LocationWeather> weather) {
        this.weather = weather;
    }

    public LocationClouds getClouds() {
        return clouds;
    }

    public void setClouds(LocationClouds clouds) {
        this.clouds = clouds;
    }

    public LocationWind getWind() {
        return wind;
    }

    public void setWind(LocationWind wind) {
        this.wind = wind;
    }

    public LocationRain getRain() {
        return rain;
    }

    public void setRain(LocationRain rain) {
        this.rain = rain;
    }

    public LocationSys getSys() {
        return sys;
    }

    public void setSys(LocationSys sys) {
        this.sys = sys;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }

}
