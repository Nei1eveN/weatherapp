package cp2.openweatherv3.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class LocationWeatherData extends RealmObject {

    @SerializedName("coord")
    @Expose
    private LocationCoord coord;

    @SerializedName("weather")
    @Expose
    private RealmList<LocationWeather> weather;

    @SerializedName("base")
    @Expose
    private String base;

    @SerializedName("main")
    @Expose
    private LocationMain main;

    @SerializedName("visibility")
    @Expose
    private Integer visibility;

    @SerializedName("wind")
    @Expose
    private LocationWind wind;

    @SerializedName("clouds")
    @Expose
    private LocationClouds clouds;

    @SerializedName("dt")
    @Expose
    private Integer dt;

    @SerializedName("sys")
    @Expose
    private LocationSys sys;

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("cod")
    @Expose
    private Integer cod;

    @SerializedName("count")
    @Expose
    private Integer count;

    @SerializedName("list")
    @Expose
    private RealmList<LocationList> list = new RealmList<>();

    public LocationCoord getCoord() {
        return coord;
    }

    public void setCoord(LocationCoord coord) {
        this.coord = coord;
    }

    public RealmList<LocationWeather> getWeather() {
        return weather;
    }

    public void setWeather(RealmList<LocationWeather> weather) {
        this.weather = weather;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public LocationMain getMain() {
        return main;
    }

    public void setMain(LocationMain main) {
        this.main = main;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public LocationWind getWind() {
        return wind;
    }

    public void setWind(LocationWind wind) {
        this.wind = wind;
    }

    public LocationClouds getClouds() {
        return clouds;
    }

    public void setClouds(LocationClouds clouds) {
        this.clouds = clouds;
    }

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public LocationSys getSys() {
        return sys;
    }

    public void setSys(LocationSys sys) {
        this.sys = sys;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCod() {
        return cod;
    }

    public void setCod(Integer cod) {
        this.cod = cod;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public RealmList<LocationList> getList() {
        return list;
    }

    public void setList(RealmList<LocationList> list) {
        this.list = list;
    }
}