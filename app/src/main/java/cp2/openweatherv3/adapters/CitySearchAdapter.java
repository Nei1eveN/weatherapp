package cp2.openweatherv3.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.openweatherv3.R;
import cp2.openweatherv3.models.search.SearchList;
import cp2.openweatherv3.views.ForecastDetailsActivity;

public class CitySearchAdapter extends RecyclerView.Adapter<CitySearchAdapter.CityViewHolder>{
    Context mContext;
    List<SearchList> citySearchList;

    public CitySearchAdapter(Context mContext, List<SearchList> searchList) {
        this.mContext = mContext;
        this.citySearchList = searchList;
    }

    class CityViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvCityName) TextView cityName;
        @BindView(R.id.tvCountryCode) TextView countryCode;
        @BindView(R.id.tvCoords) TextView coords;
        @BindView(R.id.parentLayout) RelativeLayout parentLayout;

        public CityViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cityitem, parent, false);
        return new CityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        final SearchList searchList = citySearchList.get(position);

        holder.cityName.setText(searchList.getName()); //+", "+weatherData.getSearchSys().getCountry()
        holder.countryCode.setText(searchList.getSearchSys().getCountry());
        holder.coords.setText(String.format("Coordinates: %s , %s", searchList.getSearchCoord().getLat(), searchList.getSearchCoord().getLon()));
        holder.parentLayout.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, ForecastDetailsActivity.class);
            intent.putExtra("cityName", searchList.getName());
            intent.putExtra("cityId", String.valueOf(searchList.getId()));
            ((Activity) mContext).setResult(Activity.RESULT_OK, intent);
            mContext.startActivity(intent);
            ((Activity) mContext).finish();
        });
    }

    @Override
    public int getItemCount() {
        return citySearchList.size();
    }
}