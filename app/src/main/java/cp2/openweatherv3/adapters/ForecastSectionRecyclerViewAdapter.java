package cp2.openweatherv3.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cp2.openweatherv3.R;
import cp2.openweatherv3.models.SectionModel;
import io.realm.RealmList;

public class ForecastSectionRecyclerViewAdapter extends RecyclerView.Adapter<ForecastSectionRecyclerViewAdapter.SectionViewHolder> {

    Context mContext;
    RealmList<SectionModel> sectionModelRealmList;

    public ForecastSectionRecyclerViewAdapter(Context mContext, RealmList<SectionModel> sectionModelRealmList) {
        this.mContext = mContext;
        this.sectionModelRealmList = sectionModelRealmList;
    }

    class SectionViewHolder extends RecyclerView.ViewHolder{

        TextView sectionLabel;
        RecyclerView itemRecyclerView;

        SectionViewHolder(View itemView) {
            super(itemView);
            sectionLabel = itemView.findViewById(R.id.tvHeader);
            itemRecyclerView = itemView.findViewById(R.id.sectionRecyclerView);
        }
    }

    @Override
    public SectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_section_row,parent,false);
        return new SectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SectionViewHolder holder, int position) {
        final SectionModel sectionModel = sectionModelRealmList.get(position);
        holder.sectionLabel.setText(sectionModel.getSectionLabel());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);

        //recycler view for items
        holder.itemRecyclerView.setHasFixedSize(true);
        holder.itemRecyclerView.setNestedScrollingEnabled(false);
        holder.itemRecyclerView.setLayoutManager(linearLayoutManager);

        RealmForecastAdapter realmForecastAdapter = new RealmForecastAdapter(mContext, sectionModel.getForecastListRealmList());
        holder.itemRecyclerView.setAdapter(realmForecastAdapter);
    }

    @Override
    public int getItemCount() {
        return sectionModelRealmList.size();
    }
}
