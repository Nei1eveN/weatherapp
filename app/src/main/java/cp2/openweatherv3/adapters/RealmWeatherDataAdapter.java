package cp2.openweatherv3.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.openweatherv3.models.city.CityWeatherData;
import cp2.openweatherv3.R;
import cp2.openweatherv3.views.ForecastDetailsActivity;
import cp2.openweatherv3.utils.Constants;
import io.realm.Realm;
import io.realm.RealmResults;

public class RealmWeatherDataAdapter extends RecyclerView.Adapter<RealmWeatherDataAdapter.ViewHolder> {
    private Context mContext;
    private RealmResults<CityWeatherData> dataRealmResults;
    Realm realm = Realm.getDefaultInstance();

    public RealmWeatherDataAdapter(Context mContext, RealmResults<CityWeatherData> weatherData) {
        this.mContext = mContext;
        this.dataRealmResults = weatherData;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvCity) TextView city;
        @BindView(R.id.tvCon) TextView cond;

        @BindView(R.id.clWeatherIcon) ImageView weatherIcon;
        @BindView(R.id.clCurrLocTemp) TextView currTemp;

        @BindView(R.id.body) ConstraintLayout body;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem2, parent, false);

        return new RealmWeatherDataAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CityWeatherData forecastData = dataRealmResults.get(position);
        assert forecastData != null;

        Glide.with(mContext).load(Constants.getWeatherIcon(forecastData.getWeather().get(0).getIcon())).into(holder.weatherIcon);

        holder.city.setText(String.format("%s, %s", forecastData.getName(), forecastData.getSys().getCountry()));
        holder.cond.setText(forecastData.getWeather().get(0).getDescription());
        holder.currTemp.setText(String.format("%s°C", String.valueOf(Math.round(forecastData.getMain().getTemp() - 273.15))));
        holder.body.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, ForecastDetailsActivity.class);
            intent.putExtra("cityId", String.valueOf(forecastData.getId()));
            intent.putExtra("cityName", forecastData.getName());
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return dataRealmResults.size();
    }

    public void removeItem(int position) {
        realm.beginTransaction();
        dataRealmResults.deleteFromRealm(position);
        realm.commitTransaction();
        notifyItemRemoved(position);
    }
}
