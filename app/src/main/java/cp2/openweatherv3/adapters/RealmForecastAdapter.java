package cp2.openweatherv3.adapters;

import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.openweatherv3.R;
import cp2.openweatherv3.models.forecast.ForecastList;
import cp2.openweatherv3.utils.Constants;
import io.realm.RealmList;

public class RealmForecastAdapter extends RecyclerView.Adapter<RealmForecastAdapter.ForecastViewHolder> {
    Context mContext;
    RealmList<ForecastList> forecastListRealmList;

    public RealmForecastAdapter(Context context, RealmList<ForecastList> forecastListRealmList) {
        this.mContext = context;
        this.forecastListRealmList = forecastListRealmList;
    }

    class ForecastViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvFDay) TextView day;
        @BindView(R.id.tvFDate) TextView date;
        @BindView(R.id.ivFWeatherIcon) ImageView weatherIcon;
        @BindView(R.id.tvFMaxMinTemp) TextView minTemp;
        @BindView(R.id.tvFRainChance) TextView chanceOfRain;
        @BindView(R.id.cardView) CardView cardView;

        ForecastViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_forecastitem, parent, false);
        return new ForecastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ForecastViewHolder holder, final int position) {
        final ForecastList forecastList = forecastListRealmList.get(position);

        Glide.with(mContext).asBitmap().load(Constants.getWeatherIcon(forecastList.getForecastWeather().get(0).getIcon())).into(holder.weatherIcon);

        /**
         * To fix time fetching from different cities
         */
        Date date = new Date(forecastList.getDt() * 1000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a", Locale.ENGLISH); //EEEE == Monday (whole name of day)
        final String formattedDay = simpleDateFormat.format(date);
        holder.day.setText(formattedDay);

        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH); //HH:mm
        final String formattedDate = simpleDateFormat2.format(date);
        holder.date.setText(formattedDate);

        holder.chanceOfRain.setText(String.format("%s%%", forecastList.getForecastMain().getHumidity().toString()));
        holder.minTemp.setText(String.format("%s°", String.valueOf(Math.round(forecastList.getForecastMain().getTemp() - 273.15))));

        holder.cardView.setOnClickListener(view -> {
            Log.d("realmForecastAdapter", String.valueOf(forecastList.getCityID()));
            View bottomSheet = LayoutInflater.from(mContext).inflate(R.layout.bottom_sheet, null);

            Toolbar bToolbar = bottomSheet.findViewById(R.id.bToolbar);
            bToolbar.setTitle("Forecast");
            bToolbar.setSubtitle(formattedDate+" - "+formattedDay);

            ImageView bWIcon = bottomSheet.findViewById(R.id.bWIcon);
            Glide.with(mContext).asBitmap().load(Constants.getWeatherIcon(forecastList.getForecastWeather().get(0).getIcon())).into(bWIcon);

            TextView bCurrTemp, bDesc;
            bCurrTemp = bottomSheet.findViewById(R.id.bTvTemp);
            bCurrTemp.setText(String.format("%s°", String.valueOf(Math.round(forecastList.getForecastMain().getTemp() - 273.15))));
            bDesc = bottomSheet.findViewById(R.id.bTvDesc);
            bDesc.setText(forecastList.getForecastWeather().get(0).getDescription());

            TextView spdVal, degVal;
            spdVal = bottomSheet.findViewById(R.id.bTvWSpdVal);
            spdVal.setText(String.valueOf(Math.abs(forecastList.getForecastWind().getSpeed())+" m/s"));
            degVal = bottomSheet.findViewById(R.id.bTvWDegVal);
            degVal.setText(String.format("%s %s", String.valueOf(Math.round(forecastList.getForecastWind().getDeg() - 273.15) + "°"),
                    Constants.convertDegreeToCardinalDirection(Math.round(forecastList.getForecastWind().getDeg() - 273.15))));

            TextView seaVal, groVal;
            seaVal = bottomSheet.findViewById(R.id.bTvSeaVal);
            seaVal.setText(String.format("%s m", forecastList.getForecastMain().getSeaLevel()));
            groVal = bottomSheet.findViewById(R.id.bTvGroVal);
            groVal.setText(String.format("%s sqm", forecastList.getForecastMain().getGrndLevel()));

            TextView minVal, maxVal;
            minVal = bottomSheet.findViewById(R.id.bTvMinVal);
            minVal.setText(String.valueOf(Math.round(forecastList.getForecastMain().getTempMin() - 273.15)+"°"));
            maxVal = bottomSheet.findViewById(R.id.bTvMaxVal);
            maxVal.setText(String.valueOf(Math.round(forecastList.getForecastMain().getTempMax() - 273.15)+"°"));

            TextView humVal;
            humVal = bottomSheet.findViewById(R.id.bTvHumVal);
            humVal.setText(String.valueOf(forecastList.getForecastMain().getHumidity()+"%"));

            TextView presVal;
            presVal = bottomSheet.findViewById(R.id.bTvPresVal);
            presVal.setText(String.valueOf(forecastList.getForecastMain().getPressure()+" hPa"));

            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(mContext);
            bottomSheetDialog.setContentView(bottomSheet);
            bottomSheetDialog.show();
        });
    }

    @Override
    public int getItemCount() {
        return forecastListRealmList.size();
    }
}
