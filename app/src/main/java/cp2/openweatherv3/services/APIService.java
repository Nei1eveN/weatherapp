package cp2.openweatherv3.services;

import cp2.openweatherv3.models.city.CityWeatherData;
import cp2.openweatherv3.models.forecast.ForecastData;
import cp2.openweatherv3.models.location.LocationWeatherData;
import cp2.openweatherv3.models.search.SearchListData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {
    @GET("/data/2.5/weather")
    Call<LocationWeatherData> getLocationData(@Query("lat") String LATITUDE, @Query("lon") String LONGITUDE, @Query("appid") String API_KEY);

    @GET("/data/2.5/weather")
    Call<CityWeatherData> getWeatherData(@Query("q") String cityName, @Query("APPID") String API_KEY);

    @GET("/data/2.5/weather")
    Call<CityWeatherData> getWeatherDataById(@Query("id") int cityId, @Query("APPID") String API_KEY);

    @GET("/data/2.5/forecast")
    Call<ForecastData> getForecastData(@Query("q") String cityName, @Query("APPID") String API_KEY);

    @GET("/data/2.5/forecast")
    Call<ForecastData> getForecastDataById(@Query("id") int cityId, @Query("APPID") String API_KEY);

    @GET("data/2.5/find")
    Call<SearchListData> getListData(@Query("q") String cityName, @Query("APPID") String API_KEY);

    @GET("data/2.5/find")
    Call<SearchListData> getListDataById(@Query("id") int cityId, @Query("APPID") String API_KEY);
}
