package cp2.openweatherv3.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.openweatherv3.R;
import cp2.openweatherv3.adapters.ForecastSectionRecyclerViewAdapter;
import cp2.openweatherv3.models.SectionModel;
import cp2.openweatherv3.adapters.ViewPagerAdapter;
import cp2.openweatherv3.models.city.CityWeatherData;
import cp2.openweatherv3.models.location.LocationWeatherData;
import cp2.openweatherv3.models.forecast.ForecastData;
import cp2.openweatherv3.models.forecast.ForecastList;
import cp2.openweatherv3.services.APIService;
import cp2.openweatherv3.utils.APIClient;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cp2.openweatherv3.utils.Constants.API_KEY;
import static cp2.openweatherv3.utils.Constants.REQUEST_CODE;
import static cp2.openweatherv3.utils.Constants.getWeatherIcon;

public class CurrentLocationActivity extends AppCompatActivity {

    Realm realm;
    RealmResults<LocationWeatherData> locationWeatherDataRealmResults;
    RealmList<ForecastList> forecastLocRealmList;
    RealmList<ForecastList> forecastToday = new RealmList<>();
    RealmList<ForecastList> forecastTomorrow = new RealmList<>();
    RealmList<ForecastList> forecastLater = new RealmList<>();
    RealmList<SectionModel> sectionModelRealmList = new RealmList<>();
    RealmList<SectionModel> sectionModelRealmList1 = new RealmList<>();
    RealmList<SectionModel> sectionModelRealmList2 = new RealmList<>();
    ForecastSectionRecyclerViewAdapter sectionRecyclerViewAdapter;

    @BindView(R.id.currLocCoor)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.currLocToolbar)
    Toolbar toolbar;

    @BindView(R.id.clWeatherIcon)
    ImageView currWeatherIcon;
    @BindView(R.id.clCurrLocTemp)
    TextView currTemp;
    @BindView(R.id.clCityName)
    TextView currLocName;


    @BindView(R.id.currLocViewPager)
    ViewPager viewPager;
    @BindView(R.id.currLocTabLayout)
    TabLayout tabLayout;

    Intent intent;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_location);
        ButterKnife.bind(this);

        intent = getIntent();
        String locName = intent.getStringExtra("cityName");

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(locName);
        getSupportActionBar().setSubtitle("Current Location");

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        getCurrLocData(locName);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.isIndeterminate();
        progressDialog.show();
        getForecastOfThisLoc(locName);
    }

    public void getCurrLocData(String cityName){
        locationWeatherDataRealmResults = realm.where(LocationWeatherData.class).equalTo("name", cityName).findAll();
        Glide.with(this).load(getWeatherIcon(locationWeatherDataRealmResults.get(0).getWeather().get(0).getIcon())).into(currWeatherIcon);
        currTemp.setText(String.format("%s°", String.valueOf(Math.round(locationWeatherDataRealmResults.get(0).getMain().getTemp() - 273.15))));
        currLocName.setText(locationWeatherDataRealmResults.get(0).getName());
    }

    public void getForecastOfThisLoc(String name){
        APIService forecastService = APIClient.getClient(this).create(APIService.class);
        Call<ForecastData> forecastDataCall = forecastService.getForecastData(name, API_KEY);
        forecastDataCall.enqueue(new Callback<ForecastData>() {
            @Override
            public void onResponse(Call<ForecastData> call, Response<ForecastData> response) {
                ForecastData forecastData = response.body();
                forecastLocRealmList = forecastData.getForecastList();

                for (int i = 0; i < forecastLocRealmList.size(); i++) {
                    ForecastList forecastList = new ForecastList();
                    String dateString = forecastLocRealmList.get(i).getDt()+"000";
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(Long.parseLong(dateString));

                    forecastList.setDt(forecastLocRealmList.get(i).getDt());
                    forecastList.setForecastMain(forecastLocRealmList.get(i).getForecastMain());
                    forecastList.setForecastWeather(forecastLocRealmList.get(i).getForecastWeather());
                    forecastList.setForecastWind(forecastLocRealmList.get(i).getForecastWind());

                    Calendar today = Calendar.getInstance();
                    if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
                        forecastToday.add(forecastList);
                    } else if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) + 1) {
                        forecastTomorrow.add(forecastList);
                    } else { //if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) + 2)
                        forecastLater.add(forecastList);
                    }

                    ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

                    Bundle bundleToday = new Bundle();
                    bundleToday.putInt("day", 0);
                    CurrentLocationRecycViewFragment forecastRecyclerViewFragmentToday = new CurrentLocationRecycViewFragment();
                    forecastRecyclerViewFragmentToday.setArguments(bundleToday);
                    viewPagerAdapter.addFragment(forecastRecyclerViewFragmentToday, "today");

                    Bundle bundleTomorrow = new Bundle();
                    bundleTomorrow.putInt("day", 1);
                    CurrentLocationRecycViewFragment forecastRecyclerViewFragmentTomorrow = new CurrentLocationRecycViewFragment();
                    forecastRecyclerViewFragmentTomorrow.setArguments(bundleTomorrow);
                    viewPagerAdapter.addFragment(forecastRecyclerViewFragmentTomorrow, "tomorrow");

                    Bundle bundleLater = new Bundle();
                    bundleLater.putInt("day", 2);
                    CurrentLocationRecycViewFragment forecastRecyclerViewFragmentLater = new CurrentLocationRecycViewFragment();
                    forecastRecyclerViewFragmentLater.setArguments(bundleLater);
                    viewPagerAdapter.addFragment(forecastRecyclerViewFragmentLater, "later");

                    int currentPage = viewPager.getCurrentItem();

                    viewPagerAdapter.notifyDataSetChanged();
                    viewPager.setAdapter(viewPagerAdapter);
                    viewPager.setOffscreenPageLimit(2);
                    tabLayout.setupWithViewPager(viewPager);

                    if (currentPage == 0 && forecastToday.isEmpty()) {
                        currentPage = 1;
                    }
                    viewPager.setCurrentItem(currentPage, false);
                }

                Calendar calendar = Calendar.getInstance();

                SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EEEE, MMMM d", Locale.ENGLISH); //HH:mm

                final String formattedDate = simpleDateFormat2.format(calendar.getTime());
                SectionModel sectionModel = new SectionModel();
                sectionModel.setSectionLabel(formattedDate);
                sectionModel.setForecastListRealmList(forecastToday);
                sectionModelRealmList.add(sectionModel);

                calendar.add(Calendar.DATE, 1);
                Date tomorrow = calendar.getTime();
                String formattedDate1 = simpleDateFormat2.format(tomorrow);
                SectionModel sectionModel1 = new SectionModel();
                sectionModel1.setSectionLabel(formattedDate1);
                sectionModel1.setForecastListRealmList(forecastTomorrow);
                sectionModelRealmList1.add(sectionModel1);

                calendar.add(Calendar.DATE, 1);
                Date thatDay = calendar.getTime();
                SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("MMMM d", Locale.ENGLISH); //HH:mm
                String formattedDate2 = simpleDateFormat3.format(thatDay);
                calendar.add(Calendar.DATE, 3);
                Date thatAnotherDay = calendar.getTime();
                String formattedDate3 = simpleDateFormat3.format(thatAnotherDay);
                SectionModel sectionModel2 = new SectionModel();
                sectionModel2.setSectionLabel(formattedDate2 + " - " + formattedDate3);
                sectionModel2.setForecastListRealmList(forecastLater);
                sectionModelRealmList2.add(sectionModel2);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ForecastData> call, Throwable t) {

            }
        });
    }
    public void saveDataToCityList(String name){
        APIService currentLocationService = APIClient.getClient(this).create(APIService.class);
        Call<CityWeatherData> weatherDataCall = currentLocationService.getWeatherData(name, API_KEY);
        weatherDataCall.enqueue(new Callback<CityWeatherData>() {
            @Override
            public void onResponse(Call<CityWeatherData> call, Response<CityWeatherData> response) {
                CityWeatherData weatherData = response.body();
                realm.beginTransaction();
                realm.insertOrUpdate(weatherData);
                realm.commitTransaction();
            }

            @Override
            public void onFailure(Call<CityWeatherData> call, Throwable t) {

            }
        });
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public ForecastSectionRecyclerViewAdapter getSectionAdapter(int id) {
        if (id == 0) {
            sectionRecyclerViewAdapter = new ForecastSectionRecyclerViewAdapter(this, sectionModelRealmList);
        } else if (id == 1) {
            sectionRecyclerViewAdapter = new ForecastSectionRecyclerViewAdapter(this, sectionModelRealmList1);
        } else if (id == 2) {
            sectionRecyclerViewAdapter = new ForecastSectionRecyclerViewAdapter(this, sectionModelRealmList2);
        }
        return sectionRecyclerViewAdapter;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.curr_loc_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.cityList:
                Intent intent = new Intent(this, CityActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                break;
            case R.id.addItem:
                intent = new Intent(this, SearchCityActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
                break;
            case R.id.saveItem:
                intent = getIntent();
                final String locName = intent.getStringExtra("cityName");
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Please wait...");
                progressDialog.isIndeterminate();
                progressDialog.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        saveDataToCityList(locName);
                        progressDialog.dismiss();
                    }
                }, 1500);
                Toast.makeText(this, "Location ("+locName+") has been saved to list", Toast.LENGTH_LONG).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }
}
