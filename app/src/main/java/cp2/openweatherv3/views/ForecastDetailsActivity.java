package cp2.openweatherv3.views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.openweatherv3.adapters.ForecastSectionRecyclerViewAdapter;
import cp2.openweatherv3.adapters.ViewPagerAdapter;
import cp2.openweatherv3.models.SectionModel;
import cp2.openweatherv3.models.city.CityWeatherData;
import cp2.openweatherv3.models.forecast.ForecastData;
import cp2.openweatherv3.models.forecast.ForecastList;
import cp2.openweatherv3.presenters.forecast.ForecastPresenter;
import cp2.openweatherv3.presenters.forecast.ForecastPresenterImpl;
import cp2.openweatherv3.R;
import cp2.openweatherv3.utils.Constants;
import io.realm.RealmList;
import io.realm.RealmResults;

import static cp2.openweatherv3.utils.Constants.REQUEST_CODE;

public class ForecastDetailsActivity extends AppCompatActivity implements ForecastPresenter.View {

    private static final String TAG = ForecastDetailsActivity.class.getSimpleName();

    @BindView(R.id.forecastCoor)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar detailToolbar;

    @BindView(R.id.tvCityName) TextView nameCity;
    @BindView(R.id.clWeatherIcon) ImageView weatherIcon;
    @BindView(R.id.clCurrLocTemp) TextView currentTemp;

    @BindView(R.id.clTemp) TextView minTemp;
    @BindView(R.id.tvHumVal) TextView humidityPerc;
    @BindView(R.id.clWindSpd) TextView windSpd;

    RealmList<ForecastList> forecastToday = new RealmList<>();
    RealmList<ForecastList> forecastTomorrow = new RealmList<>();
    RealmList<ForecastList> forecastLater = new RealmList<>();

    RealmList<SectionModel> sectionModelRealmList = new RealmList<>();
    RealmList<SectionModel> sectionModelRealmList1 = new RealmList<>();
    RealmList<SectionModel> sectionModelRealmList2 = new RealmList<>();
    ForecastSectionRecyclerViewAdapter sectionRecyclerViewAdapter;

    ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

    @BindView(R.id.currLocViewPager) ViewPager viewPager;
    @BindView(R.id.tabLayout) TabLayout tabLayout;

    ProgressDialog progressDialog;

    ForecastPresenter forecastPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Log.d(TAG, "Butter Knife binding");
        ButterKnife.bind(this);

        Intent intent = getIntent();
        final String cityName = intent.getStringExtra("cityName");
        final String cityId = intent.getStringExtra("cityId");

        Log.d(TAG, "initialize Toolbar");
        setSupportActionBar(detailToolbar);
        getSupportActionBar().setTitle(cityName);

        forecastPresenter = new ForecastPresenterImpl(this, getApplicationContext());
        forecastPresenter.requestCurrentData(Integer.parseInt(cityId));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_city_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.cityList:
                startActivity(new Intent(ForecastDetailsActivity.this, CityActivity.class));
                ForecastDetailsActivity.this.finish();
                break;
            case R.id.addItem:
                startActivityForResult(new Intent(ForecastDetailsActivity.this, SearchCityActivity.class), REQUEST_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "onBackPressed to MainActivity after finishing this activity");
        Intent intent = new Intent(ForecastDetailsActivity.this, CityActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        finish();
    }

    @Override
    public void setCurrentWeatherData(RealmResults<CityWeatherData> cityWeatherData) {
        Log.d("thisActivity", "setting current weather data here");
        nameCity.setText(cityWeatherData.get(0).getName());

        Glide.with(this).load(Constants.getWeatherIcon(cityWeatherData.get(0).getWeather().get(0).getIcon())).into(weatherIcon);
        currentTemp.setText(String.format("%s°", String.valueOf(Math.round(cityWeatherData.get(0).getMain().getTemp() - 273.15))));
        minTemp.setText(String.format("%s°", String.valueOf(Math.round(cityWeatherData.get(0).getMain().getTempMin() - 273.15))));

        Log.d("humidity", String.format("%s%%", String.valueOf(cityWeatherData.get(0).getMain().getHumidity())));
        humidityPerc.setText(String.format("%s%%", String.valueOf(cityWeatherData.get(0).getMain().getHumidity())));

        windSpd.setText(String.valueOf(Math.round(cityWeatherData.get(0).getWind().getSpeed()) + " m/s"));
    }

    @Override
    public void setForecastData(RealmResults<ForecastData> forecastData) {
        Log.d("forecastData", "setting data forecast here");

        Calendar today = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EEEE, MMMM d", Locale.ENGLISH); //HH:mm

        RealmList<ForecastList> forecastLists = forecastData.get(0).getForecastList();
        for (int i = 0; i < forecastLists.size(); i++){
            ForecastList forecastList = new ForecastList();
            String dateString = forecastLists.get(i).getDt() + "000";
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(Long.parseLong(dateString));

            forecastList.setCityID(forecastData.get(0).getForecastId());
            forecastList.setDt(forecastLists.get(i).getDt());
            forecastList.setForecastMain(forecastLists.get(i).getForecastMain());
            forecastList.setForecastWeather(forecastLists.get(i).getForecastWeather());
            forecastList.setForecastWind(forecastLists.get(i).getForecastWind());

            if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
                forecastToday.add(forecastList);
            } else if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) + 1) {
                forecastTomorrow.add(forecastList);
            } else {
                forecastLater.add(forecastList);
            }
        }

        /********
         *BUNDLES
         ********/
        /******
         *TODAY
         ******/
        Bundle bundleToday = new Bundle();
        bundleToday.putInt("day", 0);
        ForecastRecyclerViewFragment forecastRecyclerViewFragmentToday = new ForecastRecyclerViewFragment();
        forecastRecyclerViewFragmentToday.setArguments(bundleToday);
        viewPagerAdapter.addFragment(forecastRecyclerViewFragmentToday, "today");

        /*********
         *TOMORROW
         *********/
        Bundle bundleTomorrow = new Bundle();
        bundleTomorrow.putInt("day", 1);
        ForecastRecyclerViewFragment forecastRecyclerViewFragmentTomorrow = new ForecastRecyclerViewFragment();
        forecastRecyclerViewFragmentTomorrow.setArguments(bundleTomorrow);
        viewPagerAdapter.addFragment(forecastRecyclerViewFragmentTomorrow, "tomorrow");

        /******
         *LATER
         ******/
        Bundle bundleLater = new Bundle();
        bundleLater.putInt("day", 2);
        ForecastRecyclerViewFragment forecastRecyclerViewFragmentLater = new ForecastRecyclerViewFragment();
        forecastRecyclerViewFragmentLater.setArguments(bundleLater);
        viewPagerAdapter.addFragment(forecastRecyclerViewFragmentLater, "later");

        int currentPage = viewPager.getCurrentItem();

        viewPagerAdapter.notifyDataSetChanged();
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);

        if (currentPage == 0 && forecastToday.isEmpty()) {
            currentPage = 1;
        }
        viewPager.setCurrentItem(currentPage, false);

        /***********
         * SECTIONS
         ***********/
        /*****************
         * FORECAST TODAY
         *****************/
        final String formattedDate = simpleDateFormat2.format(calendar.getTime());
        SectionModel sectionModel = new SectionModel();
        sectionModel.setSectionLabel(formattedDate);
        sectionModel.setForecastListRealmList(forecastToday);
        sectionModelRealmList.add(sectionModel);

        /********************
         * FORECAST TOMORROW
         ********************/
        calendar.add(Calendar.DATE, 1);
        Date tomorrow = calendar.getTime();
        String formattedDate1 = simpleDateFormat2.format(tomorrow);
        SectionModel sectionModel1 = new SectionModel();
        sectionModel1.setSectionLabel(formattedDate1);
        sectionModel1.setForecastListRealmList(forecastTomorrow);
        sectionModelRealmList1.add(sectionModel1);

        /****************
         * FORECAST LATER
         ****************/
        calendar.add(Calendar.DATE, 1);
        Date thatDay = calendar.getTime();
        SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("MMMM d", Locale.ENGLISH); //HH:mm
        String formattedDate2 = simpleDateFormat3.format(thatDay);
        calendar.add(Calendar.DATE, 3);
        Date thatAnotherDay = calendar.getTime();
        String formattedDate3 = simpleDateFormat3.format(thatAnotherDay);
        SectionModel sectionModel2 = new SectionModel();
        sectionModel2.setSectionLabel(formattedDate2 + " - " + formattedDate3);
        sectionModel2.setForecastListRealmList(forecastLater);
        sectionModelRealmList2.add(sectionModel2);
    }

    @Override
    public ForecastSectionRecyclerViewAdapter getSectionAdapter(int id) {
        if (id == 0) {
            sectionRecyclerViewAdapter = new ForecastSectionRecyclerViewAdapter(this, sectionModelRealmList);
        } else if (id == 1) {
            sectionRecyclerViewAdapter = new ForecastSectionRecyclerViewAdapter(this, sectionModelRealmList1);
        } else if (id == 2) {
            sectionRecyclerViewAdapter = new ForecastSectionRecyclerViewAdapter(this, sectionModelRealmList2);
        }
        return sectionRecyclerViewAdapter;
    }

    @Override
    public void showProgress(String caption) {
        progressDialog = new ProgressDialog(ForecastDetailsActivity.this);
        progressDialog.setMessage(caption);
        progressDialog.isIndeterminate();
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }
}