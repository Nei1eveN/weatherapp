package cp2.openweatherv3.views;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.openweatherv3.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForecastRecyclerViewFragment extends Fragment {

    private static final String TAG = ForecastRecyclerViewFragment.class.getSimpleName();

    @BindView(R.id.forecastFragmentRecyclerView)
    RecyclerView forecastFragmentRecyclerView;

    Unbinder unbinder;

    public ForecastRecyclerViewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        unbinder = ButterKnife.bind(this, view);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = this.getArguments();
        ForecastDetailsActivity forecastDetailsActivity = (ForecastDetailsActivity) getActivity();

        Log.d(TAG, "setting Animation...");
        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        forecastFragmentRecyclerView.setLayoutAnimation(animation);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        forecastFragmentRecyclerView.setLayoutManager(linearLayoutManager);
        forecastFragmentRecyclerView.setAdapter(forecastDetailsActivity.getSectionAdapter(bundle.getInt("day")));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}