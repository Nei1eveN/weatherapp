package cp2.openweatherv3.views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.openweatherv3.R;
import cp2.openweatherv3.adapters.RealmWeatherDataAdapter;
import cp2.openweatherv3.models.city.CityWeatherData;
import cp2.openweatherv3.presenters.city.CityPresenter;
import cp2.openweatherv3.presenters.city.CityPresenterImpl;
import cp2.openweatherv3.utils.Constants;
import io.realm.RealmResults;

public class CityActivity extends AppCompatActivity implements CityPresenter.View { //, LocationListener

    private static final String TAG = CityActivity.class.getSimpleName();

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvNoRecFound)
    TextView noRecFound;
    @BindView(R.id.coorBody)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.constContent)
    ConstraintLayout constraintLayout;
    ConstraintSet set = new ConstraintSet();

    ProgressDialog progressDialog;
    CityPresenter cityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: started");
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Home");
        getSupportActionBar().setSubtitle("OpenWeatherMap");

        cityPresenter = new CityPresenterImpl(this, this);
        cityPresenter.requestNetworkCondition();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_city, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.addItem:
                startActivityForResult(new Intent(CityActivity.this, SearchCityActivity.class), Constants.REQUEST_CODE);
                break;
            case R.id.getCurrLoc:
                displayToast("hi");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "finishing this Activity");
        finishAffinity();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(CityActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.isIndeterminate();
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void setDataToRecycler(final RealmResults<CityWeatherData> weatherData) {
        noRecFound.setVisibility(View.GONE);
        RealmWeatherDataAdapter realmWeatherDataAdapter = new RealmWeatherDataAdapter(this, weatherData);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(realmWeatherDataAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                Log.d(TAG, "on Move");
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                Log.d(TAG, "on Swipe");
                int position = viewHolder.getAdapterPosition();
                realmWeatherDataAdapter.removeItem(position);
                if (weatherData.isEmpty()) {
                    cityPresenter.requestNetworkCondition();
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void noDataFound() {
        noRecFound.setVisibility(View.VISIBLE);
        constraintLayout.removeView(recyclerView);
        set.clone(constraintLayout);
        set.applyTo(constraintLayout);
        noRecFound.setText("No Records Found. Add Location");
    }

    private void displayToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}