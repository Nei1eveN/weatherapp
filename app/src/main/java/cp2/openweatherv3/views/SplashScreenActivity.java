package cp2.openweatherv3.views;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import butterknife.BindView;
import cp2.openweatherv3.R;

public class SplashScreenActivity extends AppCompatActivity {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        handler.postDelayed(() -> {
            Intent intent = new Intent(SplashScreenActivity.this, CityActivity.class);
            startActivity(intent);
        }, 2000);
    }
}