package cp2.openweatherv3.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.openweatherv3.R;
import cp2.openweatherv3.adapters.CitySearchAdapter;
import cp2.openweatherv3.models.search.SearchList;
import cp2.openweatherv3.presenters.search.SearchPresenter;
import cp2.openweatherv3.presenters.search.SearchPresenterImpl;

public class SearchCityActivity extends AppCompatActivity implements SearchPresenter.View {

    private static final String TAG = "SearchCityActivity";

    @BindView(R.id.cityView) RecyclerView cityRecyclerView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.etSearch) EditText search;

    SearchPresenter searchPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        searchPresenter = new SearchPresenterImpl(this, this);
        initializeSearch();
    }

    public void initializeSearch() {
        Log.d(TAG, "initializing Search");
        search.setOnEditorActionListener((textView, i, keyEvent) -> {
            try {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(search.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    searchPresenter.requestData(search.getText().toString());
                    return true;
                }
            } catch (Exception e) {
                String msg = (e.getMessage() == null) ? "failed" : e.getMessage();
                Log.i("Error", msg);
            }
            return false;
        });
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                searchPresenter.requestData(search.getText().toString());
            }
        });
    }

    @Override
    public void generateList(List<SearchList> list) {
        CitySearchAdapter citySearchAdapter = new CitySearchAdapter(this, list);
        citySearchAdapter.notifyDataSetChanged();
        cityRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        cityRecyclerView.setAdapter(citySearchAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}