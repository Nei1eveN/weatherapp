package cp2.openweatherv3.views;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.openweatherv3.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentLocationRecycViewFragment extends Fragment {

    private static final String TAG = CurrentLocationRecycViewFragment.class.getSimpleName();

    @BindView(R.id.clRecView)
    RecyclerView recyclerView;

    Unbinder unbinder;

    public CurrentLocationRecycViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_current_location_recyc_view, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setForecastFragmentRecyclerView();
    }

    public void setForecastFragmentRecyclerView(){
        Bundle bundle = this.getArguments();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        CurrentLocationActivity currentLocationActivity = (CurrentLocationActivity) getActivity();
//        forecastFragmentRecyclerView.setAdapter(forecastDetailsActivity.getRealmForecastAdapter(bundle.getInt("day")));
        recyclerView.setAdapter(currentLocationActivity.getSectionAdapter(bundle.getInt("day")));
        setAnimation();
    }

    public void setAnimation() {
        Log.d(TAG, "setting Animation...");
        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        recyclerView.setLayoutAnimation(animation);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
